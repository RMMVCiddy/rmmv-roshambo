/*:
 * @plugindesc This plugin adds the famous mini game rock-paper-scissors to your project. <Ciddy_Roshambo>
 * 
 * @version 1.0
 * @author Ciddy
 *
 * @help To play this mini game simply call the following script line from any event: SceneManager.push(Scene_Roshambo);
 * 
 * @param WinnerVariableId
 * @desc The id of the in game variable which should store the last winner (player, enemy, draw)
 * @default 1
 * @type variable
 * 
 * @param Graphics
 * @param  IconSymbol1
 * @desc The icon that should be displayed for the first symbol.
 * @default 117
 * @type number
 * @parent Graphics
 * 
 * @param  IconSymbol2
 * @desc The icon that should be displayed for the second symbol.
 * @default 68
 * @type number
 * @parent Graphics
 * 
 * @param  IconSymbol3
 * @desc The icon that should be displayed for the third symbol.
 * @default 193
 * @type number
 * @parent Graphics
 * 
 * @param Vocabulary
 * 
 * @param Symbol1 
 * @desc The text here is used to change the name of the first symbol
 * @default Schere
 * @parent Vocabulary
 * 
 * @param Symbol2 
 * @desc The text here is used to change the name of the second symbol
 * @default Stein
 * @parent Vocabulary
 * 
 * @param Symbol3
 * @desc The text here is used to change the name of the third symbol
 * @default Papier
 * @parent Vocabulary
 * 
 * @param Title
 * @desc Text that should be shown in the scene title
 * @default Schnick, Schnack, Schnuck
 * @parent Vocabulary
 * 
 * @param WinnerDefaultText
 * @desc Text that should be shown when no winner was decided yet.
 * @default Wer wird gewinnen?
 * @parent Vocabulary
 * 
 * @param PlayerWinsText
 * @desc Text that should be displayed if the player wins.
 * @default Spieler gewinnt!
 * @parent Vocabulary
 * 
 * @param EnemyWinsText
 * @desc Text that should be displayed if the enemy wins.
 * @default Gegner gewinnt!
 * @parent Vocabulary
 * 
 * @param DrawText
 * @desc Text that should be displayed if the game ends in a draw.
 * @default Unentschieden!
 * @parent Vocabulary
 * 
 */
var Imported = Imported || {};
Imported.Ciddy_Roshambo = 1.0;


Window_RoshamboCommand.prototype = Object.create(Window_Command.prototype);
Window_RoshamboCommand.prototype.constructor = Window_RoshamboCommand;

function Window_RoshamboCommand() {
    this.initialize.apply(this, arguments);
}

Scene_Roshambo.prototype = Object.create(Scene_Base.prototype);
Scene_Roshambo.prototype.constructor = Scene_Roshambo;

function Scene_Roshambo() {
    this.initialize.apply(this, arguments);
}

(function () {
var params = $plugins.filter(function (plugin) {
    return plugin.description.indexOf("Ciddy_Roshambo") > -1;
})[0].parameters;

Window_RoshamboCommand.prototype.initialize = function() {
    Window_Command.prototype.initialize.call(this, 0, 0);
};

Window_RoshamboCommand.prototype.maxRows = function() {
    return 3;
};

Window_RoshamboCommand.prototype.callHandler = function(symbol) {
    this._handler();
};

Window_RoshamboCommand.prototype.setHandler = function(method) {
    this._handler = method;
};

Window_RoshamboCommand.prototype.callOkHandler = function() {
    var symbol = this.currentSymbol();
    this._handler(symbol);
    this.close();
};

Window_RoshamboCommand.prototype.makeCommandList = function() {
    this.addCommand(params['Symbol1'], 1);
    this.addCommand(params['Symbol2'], 2);
    this.addCommand(params['Symbol3'], 3);
};

Scene_Roshambo.prototype.initialize = function() {
    Scene_Base.prototype.initialize.call(this);
    this.gameInProgress = false;
};

Scene_Roshambo.prototype.create = function() {
    Scene_Base.prototype.create.call(this);
    this.createBackground();
    this.createWindowLayer();
    this.createTitleWindow();
    this.createWinnerWindow();
    this.createAnimationWindow();
    this.createPlayWindow();
};

Scene_Roshambo.prototype.createBackground = function() {
    this._backgroundSprite = new Sprite();
    this._backgroundSprite.bitmap = SceneManager.backgroundBitmap();
    this.addChild(this._backgroundSprite);
};

Scene_Roshambo.prototype.createTitleWindow = function() {
    this._titleWindow = new Window_Base(0, 0);
    this._titleWindow.width = Graphics.width;
    this._titleWindow.height = 70;
    this._titleWindow.createContents();
    this._titleWindow.drawText(params['Title'], 0, 0, Graphics.width, 'center');
    this.addChild(this._titleWindow);
};

Scene_Roshambo.prototype.createAnimationWindow = function() {
    this._animationWindow = new Window_Base(0, this._winnerWindow.y + this._winnerWindow.height);
    this._animationWindow.width = Graphics.width;
    this._animationWindow.height = 200;
    this._animationWindow.createContents();
    this.addChild(this._animationWindow);
}

Scene_Roshambo.prototype.createWinnerWindow = function() {
    this._winnerWindow = new Window_Base(0, 0);
    this._winnerWindow.y =  this._titleWindow.y + this._titleWindow.height;
    this._winnerWindow.width = Graphics.width;
    this._winnerWindow.height = 70;
    this._winnerWindow.createContents();
    this._winnerWindow.drawText(params['WinnerDefaultText'], 0, 0, Graphics.width, 'center')
    this.addWindow(this._winnerWindow);
};

Scene_Roshambo.prototype.createPlayWindow = function() {
    this._playWindow = new Window_RoshamboCommand();
    this._playWindow.y = this._animationWindow.y + this._animationWindow.height;
    this._playWindow.setHandler(this.startRoshamboGame.bind(this));
    this._playWindow.width = 200;
    this._playWindow.height = 150;
    this.addWindow(this._playWindow);
};

Scene_Roshambo.prototype.checkWinner = function(player, enemy) {
    if (player === enemy) return 0;
    if (player === 1) {
        if (enemy === 2) return 1; 
        else return 2;
    } else if (player === 2) {
        if (enemy === 3) return 1;
        else return 2;
    } else if (player === 3) {
        if (enemy === 1) return 1;
        else return 2;
    }
};

Scene_Roshambo.prototype.symbolToText = function(symbol) {
    if (symbol === 1) return params['Symbol1'];
    else if (symbol === 2) return params['Symbol2'];
    else if (symbol === 3) return params['Symbol3'];
};

Scene_Roshambo.prototype.winnerToText = function(winner) {
    if (winner === 1) return params['EnemyWinsText'];
    else if (winner === 2) return params['PlayerWinsText'];
    else if (winner === 0) return params['DrawText'];
};

Scene_Roshambo.prototype.startRoshamboGame = function(symbol) {
    this.gameInProgress = true;
    var enemySymbol = Math.floor(Math.random() * 3) + 1;
    this.startAnimation(3, symbol, enemySymbol);
    return this.checkWinner(symbol, enemySymbol);
};

Scene_Roshambo.prototype.startAnimation = function(number, symbol, enemySymbol) {
    this.updateAnimationText(number);
    if (number >= 1) {
        var ref = this;
        setTimeout(function () {
            ref.startAnimation(number - 1, symbol, enemySymbol);
        }, 1000);
    } else {
        this._animationWindow.contents.clear();
        this.drawSymbolIcon(1, symbol);
        this.drawSymbolIcon(2, enemySymbol);
        this._winnerWindow.contents.clear();
        var winner = this.checkWinner(symbol, enemySymbol); 
        this._winnerWindow.drawText(this.winnerToText(winner), 0, 0, this._winnerWindow.width, 'center');
        if (winner === 1) AudioManager.playMe($gameSystem.defeatMe());
        if (winner=== 2) AudioManager.playMe($gameSystem.victoryMe());
        if (winner === 0) AudioManager.playMe($gameSystem.defeatMe());
        $gameVariables.setValue(Number(params['WinnerVariableId']), winner);
        var ref = this;
        setTimeout(function () {
            ref.leaveScene();
        }, 3000);
    }
};

Scene_Roshambo.prototype.leaveScene = function() {
    this.popScene();
};

Scene_Roshambo.prototype.drawSymbolIcon = function(player, symbol) {
    var ix = player === 1 ? 150 : Graphics.width - 150 - 64;
    var iy = this._animationWindow.height / 2 - 64;
    this._animationWindow.drawIcon(symbol === 1 ? params['IconSymbol1'] : symbol === 2 ? params['IconSymbol2'] : params['IconSymbol3'], ix, iy);
    var textWidth = this._animationWindow.textWidth(this.symbolToText(symbol)) / 2;
    this._animationWindow.drawText(this.symbolToText(symbol), ix - textWidth / 2, iy + 30, Graphics.width);
};

Scene_Roshambo.prototype.updateAnimationText = function(text) {
    this._animationWindow.contents.clear();
    var textWidth = this._animationWindow.textWidth(text);
    this._animationWindow.drawText(text , 0 - textWidth, 0, Graphics.width, 'center');
};

Scene_Roshambo.prototype.update = function() {
    Scene_Base.prototype.update.call(this);
    if (Input.isTriggered('cancel') && this.gameInProgress == false) {
        SoundManager.playCancel();
        this.popScene();
    }
};
})();